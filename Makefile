# CXX = g++
# CXXFLAGS = -Wall -w -Wextra -std=c++11
# LDFLAGS = -lpthread

CXX = g++
CXXFLAGS = -Wall -w -Wextra -std=c++11 -fsanitize=address -g
LDFLAGS = -fsanitize=address -lpthread

# 项目名称
TARGET = ospf_program

# 子目录
SRCDIR = src
CONFIGDIR = $(SRCDIR)/config
SCHEDULERDIR = $(SRCDIR)/scheduler
TOOLSDIR = $(SRCDIR)/tools
UNITDIR = $(SRCDIR)/unit

# 源文件
SOURCES = $(SRCDIR)/main.cpp \
          $(wildcard $(CONFIGDIR)/*.cpp) \
          $(wildcard $(SCHEDULERDIR)/*.cpp) \
          $(wildcard $(TOOLSDIR)/*.cpp) \
          $(wildcard $(UNITDIR)/*.cpp)

# 目标文件
OBJECTS = $(SOURCES:.cpp=.o)

# 默认目标
all: $(TARGET)

# 链接目标文件生成可执行文件
$(TARGET): $(OBJECTS)
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

# 编译源文件生成目标文件
%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

# 清理生成的文件
clean:
	rm -f $(TARGET) $(OBJECTS)

# 伪目标
.PHONY: all clean
