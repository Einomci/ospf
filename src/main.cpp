#include "config/setting.h"

#include "unit/interface.h"
#include "scheduler/Sender.h"
#include "scheduler/Reciever.h"
#include "unit/lsdb.h"
#include "unit/routing.h"

#include <pthread.h>
#include <string>
#include <vector>

// 全局对象
LSDB lsdb;
RouteTable route_manager;

int main(int argc, char** argv) {
    // 线程向量
    std::vector<pthread_t> hello_sender_threads;
    std::vector<pthread_t> receiver_threads;

    // 网口配置信息
    struct InterfaceConfig {
        std::string ip;
        std::string nic_name;
        int nic_id;
        int cost;
    };

    std::vector<InterfaceConfig> interface_configs = {
        // {"192.168.127.142", "ens33", 33},
        {"192.168.123.128", "ens38", 38, 60},
        {"192.168.61.128", "ens37", 37, 1}

    };

    // 初始化每个网口
    for (const auto& config : interface_configs) {
        Interface* iface = new Interface();
        iface->ip = ntohl(inet_addr(config.ip.c_str()));
        iface->nic_name = config.nic_name.c_str();
        iface->nic_id = config.nic_id;
        iface->cost = config.cost;
        myconfigs::interfaces.push_back(iface);
        myconfigs::ip2interface[iface->ip] = iface;
        iface->trigger_init();

        // 初始化线程全局属性
        pthread_attr_init(&myconfigs::thread_attr);
        pthread_attr_setdetachstate(&myconfigs::thread_attr, PTHREAD_CREATE_DETACHED);
        // 初始化lsa_seq_lock
        pthread_mutex_init(&lsa_seq_lock, NULL);

        // 创建并启动线程
        pthread_t hello_sender_thread;
        pthread_t receiver_thread;
        pthread_create(&hello_sender_thread, NULL, threadSendHelloPackets, iface);
        pthread_create(&receiver_thread, NULL, threadRecvPackets, iface);

        // 存储线程句柄
        hello_sender_threads.push_back(hello_sender_thread);
        receiver_threads.push_back(receiver_thread);
    }

    // 等待所有线程完成
    for (pthread_t& thread : hello_sender_threads) {
        pthread_join(thread, NULL);
    }

    for (pthread_t& thread : receiver_threads) {
        pthread_join(thread, NULL);
    }

    printf("ospf close\n");
    return 0;
}
