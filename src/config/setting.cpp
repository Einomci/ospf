#include "setting.h"

namespace myconfigs {
    uint32_t router_id = ntohl(inet_addr("15.15.15.15"));
    std::vector<Interface*> interfaces;
    std::map<uint32_t, Interface*> ip2interface;
    pthread_attr_t thread_attr;
}

struct in_addr ipaddr_tmp;
int lsa_seq_cnt = 0;
pthread_mutex_t lsa_seq_lock;
bool to_exit = false;
