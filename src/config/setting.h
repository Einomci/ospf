#ifndef SETTINGS_H
#define SETTINGS_H

#include <stdint.h>
#include <vector>
#include <map>
#include <pthread.h>
#include <arpa/inet.h> // contains <netinet/in.h>
#include "../unit/interface.h"

#define DEBUG 1

namespace myconfigs {
    extern uint32_t router_id;
    extern std::vector<Interface*> interfaces;
    extern std::map<uint32_t, Interface*> ip2interface;
    extern pthread_attr_t thread_attr;
} // namespace myconfigs

extern struct in_addr ipaddr_tmp;
extern int lsa_seq_cnt;
extern pthread_mutex_t lsa_seq_lock;
extern bool to_exit;

#endif // SETTINGS_H
