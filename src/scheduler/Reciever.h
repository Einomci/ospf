#ifndef RECIEVER_H
#define RECIEVER_H

#include "../unit/interface.h"
#include "../unit/lsdb.h"
#include "../config/setting.h"

#include <netinet/ip.h>  // for struct iphdr
#include <string.h>      // for memcpy
#include <unistd.h>      // for close
#include <netinet/if_ether.h> 
#include <iostream>


void sendPackets(const char* ospf_data, int data_len, uint8_t type, uint32_t dst_ip, Interface* interface);

inline void forwardPacket(const char* packet, int packet_len, uint32_t dst_ip, Interface* interface) {
    int sockfd;
    struct sockaddr_in dest_addr;
    
    // 创建套接字
    if ((sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
        perror("socket");
        return;
    }

    // 设置目的地址
    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.sin_family = AF_INET;
    dest_addr.sin_addr.s_addr = dst_ip;

    // 发送报文
    if (sendto(sockfd, packet, packet_len, 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr)) < 0) {
        perror("sendto");
    }

    close(sockfd);
}

inline void* threadRecvPackets(void *intf) {
Interface *interface = (Interface*) intf;
    int socket_fd;
    const char *multicastIP ="224.0.0.5"; // 0SPF AlLSPFRouters 地址
    struct ip_mreq mreq{};
    struct iphdr *ip_header;
    struct in_addr src, dst;
#define RECV_LEN 1514
    char* frame_rcv = (char*)malloc(RECV_LEN);
    char* packet_rcv;
    if (interface->nic_id == 33) {
        if ((socket_fd = socket(AF_INET, SOCK_RAW, 89)) < 0) {
            perror("[Thread]RecvPacket: socket_fd init");
        }
        mreq.imr_multiaddr.s_addr = inet_addr(multicastIP);// 绑定到特定网络接口
        mreq.imr_interface.s_addr = htonl(INADDR_ANY);//或者指定具体的接口 IP// 加入多播组
        if (setsockopt(socket_fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq))<0) {
            perror("Setsockopt IP_ADD_MEMBERSHIP failed");close(socket_fd);
        }
        packet_rcv = frame_rcv;
    } else {
        struct ifreq ifr;
        memset(&ifr, 0, sizeof(ifr));
        strncpy(ifr.ifr_name, interface->nic_name, IFNAMSIZ - 1);
        if ((socket_fd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_IP))) < 0) {
            perror("[Thread]RecvPacket: socket_fd init");
        }
        if (setsockopt(socket_fd, SOL_SOCKET, SO_BINDTODEVICE, (void *)&ifr, sizeof(ifr)) < 0) {     // 使用SO_BINDTODEVICE将套接字绑定到指定接口
            perror("[Thread]RecvPacket: setsockopt SO_BINDTODEVICE");
            close(socket_fd);
        }
        packet_rcv = frame_rcv + sizeof(struct ethhdr);     // 将frame_rcv指针偏移以指向以太网头部之后的数据
    }

    uint32_t dst_ip;
    if (interface->nic_id == 38) {
        dst_ip = inet_addr("192.168.61.7");
    } else {
        dst_ip = inet_addr("192.168.123.7");
    }

    while (true) {
        if (to_exit) {
            break;
        }

        memset(frame_rcv, 0, RECV_LEN);
        int recv_size = recv(socket_fd, frame_rcv, RECV_LEN, 0);
        
        /* check IP Header : filter  */
        ip_header = (struct iphdr*)packet_rcv;
        // 1. not OSPF packet
        if (ip_header->protocol != 89) {
            continue;
            printf("\n\n\n\n\n____________OTHER____________\n\n\n", interface->nic_name);
            forwardPacket(packet_rcv, recv_size, dst_ip, interface);
            
        } else {
            printf("!!!!!!!!!!!%s OHHHHHHHHHH!OSPF\n", interface->nic_name);
        }
        // 2. src_ip or dst_ip don't fit
        in_addr_t src_ip = ntohl(*(uint32_t*)(packet_rcv + IPHDR_SRCIP));
        in_addr_t dst_ip = ntohl(*(uint32_t*)(packet_rcv + IPHDR_DSTIP));
        if ((dst_ip != interface->ip && dst_ip != ntohl(inet_addr("224.0.0.5"))) ||
            src_ip == interface->ip) {
            continue;
        }

        #ifdef DEBUG
            printf("[Thread]RecvPacket: recv one");
            src.s_addr = htonl(src_ip);
            dst.s_addr = htonl(dst_ip);
            printf(" src:%s,", inet_ntoa(src)); // inet_ntoa() -> char* is static
            printf(" dst:%s\n", inet_ntoa(dst));
        #endif

        OSPFHeader* ospf_header = (OSPFHeader*)(packet_rcv + IPHDR_LEN);
        /* translate : net to host */
        ospf_header->packet_length = ntohs(ospf_header->packet_length);
        ospf_header->router_id     = ntohl(ospf_header->router_id    );
        ospf_header->area_id       = ntohl(ospf_header->area_id      );
        ospf_header->checksum      = ntohl(ospf_header->checksum     );

        if (ospf_header->type == T_HELLO) {
        #ifdef DEBUG
            printf("[Thread]RecvPacket: Hello packet\n");
        #endif
            OSPFHello* ospf_hello = (OSPFHello*)(packet_rcv + IPHDR_LEN + OSPFHDR_LEN);
            Neighbor* neighbor;

            if ((neighbor = interface->find_neighbor(src_ip)) == nullptr) {
                neighbor = interface->create_neighbor(src_ip);
            }
            neighbor->id   = ospf_header->router_id;
            uint32_t prev_ndr   = neighbor->ndr;
            uint32_t prev_nbdr  = neighbor->nbdr;
            neighbor->ndr  = ntohl(ospf_hello->designated_router);
            neighbor->nbdr = ntohl(ospf_hello->backup_designated_router);
            if (neighbor->ndr == 0) {
                interface->real_dr = interface->ip;
                interface->real_bdr = neighbor->ip;
            } else {
                interface->real_dr = neighbor->ndr;
                interface->real_bdr = neighbor->nbdr;                
            } 
            neighbor->priority = ntohl(ospf_hello->rtr_pri);

            neighbor->handle_hello_received();

            bool to_2way = false;
            /* Decide 1way or 2way: check whether Hello attached-info has self-routerid */ 
            uint32_t* ospf_attach = (uint32_t*)(packet_rcv + IPHDR_LEN + OSPF_HELLO_LEN);
            uint32_t* ospf_end = (uint32_t*)(packet_rcv + IPHDR_LEN + ospf_header->packet_length);
            for (;ospf_attach != ospf_end; ++ospf_attach) {
                if (*ospf_attach == htonl(myconfigs::router_id)) {
                    to_2way = true;
                    neighbor->handle_2way_received(interface);
                    break;
                }
            }
            if (!to_2way) {
                neighbor->handle_1way_received();
                continue; // finish packet process
            }

            /* Decide NeighborChange / BackupSeen Event */
            /* == about DR == */
            if (neighbor->ndr  == neighbor->ip &&
                neighbor->nbdr == 0x00000000 &&
                interface->state == InterfaceState::S_WAITING
                ) {
                interface->trigger_bdr_detected();
            } else 
            if ((prev_ndr == neighbor->ip) ^ (neighbor->ndr == neighbor->ip)) {
                //   former declare dr   ^  now declare dr
                interface->trigger_neighbor_change();
            }
            /* == about BDR == */
            if (neighbor->nbdr == neighbor->ip &&
                interface->state == InterfaceState::S_WAITING
                ) {
                interface->trigger_bdr_detected(); 
            } else
            if ((prev_nbdr == neighbor->ip) ^ (neighbor->nbdr == neighbor->ip)) {
                //   former declare bdr   ^  now declare bdr
                interface->trigger_neighbor_change();
            }
        }
        else
        if (ospf_header->type == T_DD) {
        #ifdef DEBUG
            printf("[Thread]RecvPacket: DD packet\n");
        #endif
            OSPFDD* ospf_dd = (OSPFDD*)(packet_rcv + IPHDR_LEN + OSPFHDR_LEN);
            Neighbor* neighbor = interface->find_neighbor(src_ip);
            
            bool is_accepted = false;
            bool is_dup = false;
            uint32_t seq_num = ntohl(ospf_dd->sequence_number);
            if (neighbor->last_dd_seq_num == seq_num) {
                is_dup = true;
            #ifdef DEBUG
                printf("[Thread]RecvPacket: DD packet duplicated\n");
            #endif
            } else {
                neighbor->last_dd_seq_num = seq_num;
            }

            /* Dealing DD depending on neighbor's state */
            dd_switch:
            switch (neighbor->state) {
                case NeighborState::S_INIT: {
                    neighbor->handle_2way_received(interface);
                    goto dd_switch; // depend on the result of the event
                    break;
                }
                case NeighborState::S_EXSTART: {
                    if (ospf_dd->b_M && ospf_dd->b_I && ospf_dd->b_MS
                        && neighbor->id > myconfigs::router_id) {
                        // receive dd: 1 1 1 bigger_id
                        /* confirm neighbor is master */
                        #ifdef DEBUG
                            printf("neighbor %x declare master, ", neighbor->id);
                            printf("interface %x agree => ", myconfigs::router_id);
                            printf("neighbor is master\n");
                        #endif
                        neighbor->is_master = true;
                        neighbor->dd_seq_num = seq_num;
                    } else
                    if (!ospf_dd->b_I && !ospf_dd->b_MS
                        && ospf_dd->sequence_number == seq_num
                        && neighbor->id < myconfigs::router_id) {
                        // receive dd: x 0 0 smaller_id
                        /* confirm neighbor is slave */
                        #ifdef DEBUG
                            printf("neighbor %x agree slave, ", neighbor->id);
                            printf("interface %x confirm\n => ", interface->ip);
                            printf("self is master\n");
                        #endif
                        neighbor->is_master = false;
                    } else {
                        /* ignore the packet */
                        #ifdef DEBUG
                            std::cout << "Debug Info: ospf_dd->b_M: " << ospf_dd->b_M << std::endl;
                            std::cout << "Debug Info: ospf_dd->b_I: " << ospf_dd->b_I << std::endl;
                            std::cout << "Debug Info: ospf_dd->b_MS: " << ospf_dd->b_MS << std::endl;
                            std::cout << "Debug Info: neighbor->id: " << neighbor->id << std::endl;
                            std::cout << "Debug Info: myconfigs::router_id: " << myconfigs::router_id << std::endl;
                            printf("don't agree, ignore\n");
                        #endif
                        continue; 
                    }
                    neighbor->handle_negotiation_done();
                    // received dd may contain lsa_header now, need parse again in S_EXCHANGE
                    goto dd_switch; 
                    break;
                }
                case NeighborState::S_EXCHANGE: {
                    if (is_dup) {
                        #ifdef DEBUG
                            printf("duplicated dd packet: ignore\n");
                        #endif
                        if (neighbor->is_master) {
                            // neighbor is master -> host interface is slave
                            sendPackets(neighbor->last_dd_data, neighbor->last_dd_data_len, T_DD, neighbor->ip, neighbor->host_interface); 
                            printf("\n\n\n-------------------recv 221---------------------------\n\n\n");
                        }
                        continue; // ignore the packet
                    }
                    if ((ospf_dd->b_MS ^ neighbor->is_master)) {
                        // negotiation is done, b_I should not be 1 ; b_MS should fit negotiation result
                        #ifdef DEBUG
                            printf("mismatch: b_I should not be 1 or b_MS should fit negotiation result\n");
                        #endif
                        neighbor->handle_seq_mismatch(); // TODO
                        continue;
                    }
                    if ((neighbor->is_master && seq_num == neighbor->dd_seq_num) ||
                        (!neighbor->is_master && seq_num == neighbor->dd_seq_num)) {
                        // dd packet's seq_num is legal: so we can recv it and analysis (it may has lsa_header)
                        #ifdef DEBUG
                            printf("neighbor is master ? : %d\n",neighbor->is_master);
                            printf("recv seq_num = %d", seq_num);
                        #endif
                        is_accepted = true;
                    } else {
                        #ifdef DEBUG
                            printf("mismatch: dd packet's seq_num is illegal\n");
                            printf("neighbor->is_master ? %d\n", neighbor->is_master);
                            printf("seq_num = %d\n", seq_num);
                        #endif
                        
                        neighbor->handle_seq_mismatch();
                        continue;
                    }
                    break;
                }
                case NeighborState::S_LOADING:
                case NeighborState::S_FULL: {
                    /* finish dd exchange */
                    // only deal with duplicate or mismatch
                    if (is_dup) {
                        if (neighbor->is_master) {
                            // neighbor is master -> host interface is slave
                            sendPackets(neighbor->last_dd_data, neighbor->last_dd_data_len, T_DD, neighbor->ip, neighbor->host_interface); 
                            printf("\n\n\n-------------------recv 262---------------------------\n\n\n");
                        }
                        continue; // ignore the packet
                    }
                    if (ospf_dd->b_I || (ospf_dd->b_MS ^ neighbor->is_master)) {
                        neighbor->handle_seq_mismatch(); // TODO
                        continue;
                    }
                    break;
                }
                default: ;
            }

            if (is_accepted) {
                /* 1. Receive DD packet and update link_state_req_list */
                LSAHeader* lsa_header_rcv = (LSAHeader*)(packet_rcv + IPHDR_LEN + OSPF_DD_LEN);
                LSAHeader* lsa_header_end = (LSAHeader*)(packet_rcv + IPHDR_LEN + ospf_header->packet_length);
                while (lsa_header_rcv != lsa_header_end) {
                    LSAHeader lsa_header;
                    lsa_header.advertising_router = ntohl(lsa_header_rcv->advertising_router);
                    lsa_header.link_state_id = ntohl(lsa_header_rcv->link_state_id);
                    lsa_header.ls_sequence_number = ntohl(lsa_header_rcv->ls_sequence_number);
                    lsa_header.ls_type = lsa_header_rcv->ls_type;
                    /* add to link_state_req_list if lsdb do not have the lsa */
                    if (lsa_header.ls_type == LSA_ROUTER) {
                        if (lsdb.getRouterLSA(lsa_header.link_state_id, lsa_header.advertising_router) == nullptr) {
                            neighbor->link_state_req_list.push_back(lsa_header);
                        }
                    } else if (lsa_header.ls_type == LSA_NETWORK) {
                        if (lsdb.getNetworkLSA(lsa_header.link_state_id, lsa_header.advertising_router) == nullptr) {
                            neighbor->link_state_req_list.push_back(lsa_header);
                        }
                    }

                    lsa_header_rcv += 1;
                }

                /* 2. Reply to the DD packet received */
                #ifdef DEBUG
                    printf("making reply dd packet.\n");
                #endif
                char* data_ack = (char*)malloc(1024);
                int data_len = 0;

                OSPFDD* dd_ack = (OSPFDD*)data_ack;
                memset(dd_ack, 0, sizeof(OSPFDD));
                data_len += sizeof(OSPFDD);
                dd_ack->options = 0x02;
                dd_ack->interface_MTU = htons(neighbor->host_interface->mtu);
                dd_ack->b_MS = neighbor->is_master ? 0 : 1;
                // operation is different between master and slave
                if (neighbor->is_master) {
                    /* interface is slave */
                    neighbor->dd_seq_num = seq_num + 1;
                    dd_ack->sequence_number = htonl(seq_num);
                    printf("ACK ACK ACK ACK %d\n", seq_num);
                    printf("neighbor->dd_seq_num except next = %d\n", seq_num + 1);
                    // add lsa_header
                    LSAHeader* write_lsa_header = (LSAHeader*)(data_ack + sizeof(OSPFDD));
                    int lsa_cnt = 0;
                    while (neighbor->db_summary_list.size() > 0) {
                        if (lsa_cnt >= 10) break;   // simply limit to 10 lsa (in fact may it depend on MTU, up to 100)
                        
                        LSAHeader& lsa_h = neighbor->db_summary_list.front();
                        lsa_h.host2net();
                        memcpy(write_lsa_header, &lsa_h, LSAHDR_LEN);
                        
                        write_lsa_header += 1;
                        lsa_cnt += 1;
                        data_len += LSAHDR_LEN;
                        neighbor->db_summary_list.pop_front();
                    }
                    dd_ack->b_M = (neighbor->db_summary_list.size() == 0) ? 0 : 1;
                    // send ack packet
                    printf("\n\n\n-------------------recv 336---------------------------\n\n\n");            
                    sendPackets(data_ack, data_len, T_DD, neighbor->ip, neighbor->host_interface);
                    memcpy(neighbor->last_dd_data, data_ack, data_len);
                    neighbor->last_dd_data_len = data_len;
                    // evoke event if there's no more dd packet
                    if (dd_ack->b_M == 0 && ospf_dd->b_M == 0) {
                        neighbor->handle_exchange_done();
                    }
                    free(data_ack);
                } else {
                    /* interface is master */
                    // receive ack of last dd, stop rxmt of this packet
                    if (neighbor->link_state_rxmt_map.count(neighbor->dd_seq_num) > 0) {
                        // check for safety, although there's no need to check here
                        #ifdef DEBUG
                            printf("...recv dd ack, delete rxmt of packet...\n");
                        #endif
                        interface->rxmtter.delPacketData(neighbor->link_state_rxmt_map[neighbor->dd_seq_num]);
                    }
                    neighbor->dd_seq_num += 1;
                    dd_ack->sequence_number = htonl(neighbor->dd_seq_num);

                    // evoke event if there's no more dd packet
                    // [Attention]: Master would always evoke this event after Slave  
                    if (neighbor->db_summary_list.size() == 0 && ospf_dd->b_M == 0) {
                        neighbor->handle_exchange_done();
                        free(data_ack);
                        continue;
                    }
                    // add lsa_header
                    LSAHeader* write_lsa_header = (LSAHeader*)(data_ack + sizeof(OSPFDD));
                    int lsa_cnt = 0;
                    #ifdef DEBUG
                        printf("adding lsa_header...\n");
                    #endif
                    while (neighbor->db_summary_list.size() > 0) {
                        if (lsa_cnt >= 10) break;   // simply limit to 10 lsa (in fact may it depend on MTU, up to 100)

                        LSAHeader& lsa_h = neighbor->db_summary_list.front();
                        memcpy(write_lsa_header, &lsa_h, LSAHDR_LEN);
                        #ifdef DEBUG
                            lsa_h.printInfo();
                        #endif

                        write_lsa_header += 1;
                        lsa_cnt += 1;
                        data_len += LSAHDR_LEN;
                        neighbor->db_summary_list.pop_front();
                    }
                    dd_ack->b_M = (neighbor->db_summary_list.size() == 0) ? 0 : 1;
                    // send ack packet
                    sendPackets(data_ack, data_len, T_DD, neighbor->ip, neighbor->host_interface);
                    printf("\n\n\n-------------------recv 388---------------------------\n\n\n");
                    uint32_t pdata_id = interface->rxmtter.addPacketData(
                        PacketData(data_ack, data_len, T_DD, neighbor->ip, interface->rxmt_interval)
                    );
                    neighbor->link_state_rxmt_map[neighbor->dd_seq_num] = pdata_id;
                }
                
            }
        }
        else
        if (ospf_header->type == T_LSR) {
        #ifdef DEBUG
            printf("[Thread]RecvPacket: LSR packet\n");
        #endif
            OSPFLSR* lsr_rcv = (OSPFLSR*)(packet_rcv + IPHDR_LEN + OSPFHDR_LEN);
            OSPFLSR* lsr_end = (OSPFLSR*)(packet_rcv + IPHDR_LEN + ospf_header->packet_length);
            Neighbor* neighbor = interface->find_neighbor(src_ip);

            // check neighbor state
            if (neighbor->state < NeighborState::S_EXCHANGE) {
                // only S_EXCHANGE, S_LOADING, S_FULL can receive LSR
            #ifdef DEBUG
                printf("current neighbor state cannot receive LSR packet.\n");
            #endif
                continue;
            }

            char* lsu_data_ack = (char*)malloc(2048); /* lsu_data_ack = lsu_body + lsu_attached */
            // OSPFLSU body ： size
            OSPFLSU* lsu_body = (OSPFLSU*)lsu_data_ack;
            memset(lsu_body, 0, sizeof(OSPFLSU));
            lsu_body->num = 0;   // redundent, just for consistency
            // attached : lsa
            char* lsu_attached = lsu_data_ack + sizeof(OSPFLSU);
            int lsa_cnt = 0;
            while (lsr_rcv != lsr_end) {
                lsr_rcv->net2host();

                if (lsr_rcv->type == LSA_ROUTER) {
                    LSARouter* router_lsa = lsdb.getRouterLSA(lsr_rcv->state_id, lsr_rcv->adverising_router);
                    if (router_lsa == nullptr) {
                        neighbor->handle_bad_ls_request();
                        free(lsu_data_ack);
                        goto after_dealing; // TODO: just an expedient
                    } else {
                        char* lsa_packet = router_lsa->toRouterLSA();
                        memcpy(lsu_attached, lsa_packet, router_lsa->size());
                        delete[] lsa_packet;   // release "new char[size()]" from toRouterLSA()
                        lsu_attached += router_lsa->size();
                    }
                } else if (lsr_rcv->type == LSA_NETWORK) {
                    LSANetwork* network_lsa = lsdb.getNetworkLSA(lsr_rcv->state_id, lsr_rcv->adverising_router);
                    if (network_lsa == nullptr) {
                        neighbor->handle_bad_ls_request();
                        free(lsu_data_ack);
                        goto after_dealing; // TODO: just an expedient
                    } else {
                        char* lsa_packet = network_lsa->toNetworkLSA();
                        memcpy(lsu_attached, lsa_packet, network_lsa->size());
                        delete[] lsa_packet;
                        lsu_attached += network_lsa->size();
                    }
                }

                lsr_rcv += 1;
                lsa_cnt += 1;
            }
            lsu_body->num = htonl(lsa_cnt);
            sendPackets(lsu_data_ack,(lsu_attached - lsu_data_ack), T_LSU, src_ip, interface);
        }
        else
        if (ospf_header->type == T_LSU) {
        #ifdef DEBUG
            printf("[Thread]RecvPacket: LSU packet\n");
        #endif
            Neighbor* neighbor = nullptr;
            bool is_specified = false;
            // check dst : if dst_ip == interface ip, then its specified from interface's LSR
            if (dst_ip == interface->ip) {
                is_specified = true;
                neighbor = interface->find_neighbor(src_ip);
            }

            OSPFLSU* ospf_lsu = (OSPFLSU*)(packet_rcv + IPHDR_LEN + OSPFHDR_LEN);
            int lsa_num = ntohl(ospf_lsu->num);

            char* lsack_data = (char*)malloc(1024); 
            LSAHeader* lsa_header_writer = (LSAHeader*)lsack_data;
            char* lsa_net_ptr = (char*)(ospf_lsu) + sizeof(OSPFLSU); // +4
            for (int i = 0; i < lsa_num; ++i) {
                // receive lsa form ospf_lsu
                lsdb.addLSA(lsa_net_ptr);
                
                LSAHeader* lsa_header = (LSAHeader*)lsa_net_ptr;
                if (is_specified) {
                    // remove its header from link_state_req_list if has one
                    neighbor->reqListRemove(ntohl(lsa_header->link_state_id), ntohl(lsa_header->advertising_router));
                }

                memcpy(lsa_header_writer, lsa_header, sizeof(LSAHeader));
                lsa_header_writer += 1;
                lsa_net_ptr += ntohs(lsa_header->length);
                // sendPackets((char*)lsa_header, LSAHDR_LEN, T_LSAck, ntohl(inet_addr("224.0.0.5")), interface);
            }
            // TODO: check num and verify
            sendPackets(lsack_data, LSAHDR_LEN * lsa_num, T_LSAck, ntohl(inet_addr("224.0.0.5")), interface);
            free(lsack_data);
        }

        after_dealing:;
    }

    // free(packet_rcv);
    pthread_exit(NULL);
#undef RECV_LEN
    return nullptr;
}

#endif // REVIEVER_H