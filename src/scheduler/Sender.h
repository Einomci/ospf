#ifndef PACKET_MANAGE_H
#define PACKET_MANAGE_H

#include "../unit/interface.h"
#include "../config/setting.h"

#include <net/if.h>  
#include <string.h>      // for memcpy
#include <unistd.h>      // for close

// 函数声明
uint16_t crc_checksum(const void* data, size_t len);
void* threadSendHelloPackets(void* intf);
void* threadSendEmptyDDPackets(void* nbr);
void* threadSendLSRPackets(void* nbr);

// 函数定义
inline uint16_t crc_checksum(const void* data, size_t len) {
    uint32_t sum = 0;
    const uint16_t* ptr = static_cast<const uint16_t*>(data);

    // accumulate per 16 bits (2 Bytes)
    for (size_t i = 0; i < len / 2; ++i) {
        sum += *ptr++;
    }
    if (len & 1) {  // if odd len
        sum += static_cast<const uint8_t*>(data)[len - 1];
    }

    sum = (sum >> 16) + (sum & 0xffff);
    sum += (sum >> 16);
    return static_cast<uint16_t>(~sum);
}

inline void sendPackets(const char* ospf_data, int data_len, uint8_t type, uint32_t dst_ip, Interface* interface) {
// #ifdef DEBUG
    //     printf("...try to use [sendPackets]: data_len - %d, type - %d...\n", data_len, type);
    // #endif
    int socket_fd;
    if ((socket_fd = socket(AF_INET, SOCK_RAW, IPPROTO_OSPF)) < 0) {
        perror("SendPacket: socket_fd init");
    }

    /* Bind sockets to certain Network Interface */
    struct ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));
    strcpy(ifr.ifr_name, interface->nic_name);
    if (setsockopt(socket_fd, SOL_SOCKET, SO_BINDTODEVICE, &ifr, sizeof(ifr)) < 0) {
        perror("SendPacket: setsockopt");
    }

    /* Set Dst Address : certain ip */
    struct sockaddr_in dst_sockaddr;
    memset(&dst_sockaddr, 0, sizeof(dst_sockaddr));
    dst_sockaddr.sin_family = AF_INET;
    dst_sockaddr.sin_addr.s_addr = htonl(dst_ip);

    char* packet = (char*)malloc(1500);
    size_t packet_len = OSPFHDR_LEN + data_len;
    /* OSPF Header */
    OSPFHeader* ospf_header = (OSPFHeader*)packet;
    ospf_header->version = 2;
    ospf_header->type = type;
    ospf_header->packet_length = htons(packet_len);
    ospf_header->router_id = htonl(myconfigs::router_id);
    ospf_header->area_id   = htonl(interface->area_id);
    ospf_header->checksum  = 0;
    ospf_header->autype = 0;
    ospf_header->authentication[0] = 0;
    ospf_header->authentication[1] = 0;

    /* OSPF Data */
    memcpy(packet + OSPFHDR_LEN, ospf_data, data_len);

    // calculte checksum
    ospf_header->checksum = crc_checksum(ospf_header, packet_len);

    /* Send OSPF Packet */
    if (sendto(socket_fd, packet, packet_len, 0, (struct sockaddr*)&dst_sockaddr, sizeof(dst_sockaddr)) < 0) {
            perror("SendPacket: sendto");
    } 
#ifdef DEBUG
    else {
        printf("SendPacket: type %d send success, len %d\n", type, packet_len);
    }
#endif

    free(packet);
}

inline void* threadSendHelloPackets(void* intf) {
Interface *interface = (Interface*) intf;
    int socket_fd;
    if ((socket_fd = socket(AF_INET, SOCK_RAW, IPPROTO_OSPF)) < 0) {
        perror("[Thread]SendHelloPacket: socket_fd init");
    }

    /* Bind sockets to certain Network Interface */
    struct ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));
    strcpy(ifr.ifr_name, interface->nic_name);
    if (setsockopt(socket_fd, SOL_SOCKET, SO_BINDTODEVICE, &ifr, sizeof(ifr)) < 0) {
        perror("[Thread]SendHelloPacket: setsockopt");
    }

    /* Set Address : multicast */
    struct sockaddr_in dst_sockaddr;
    memset(&dst_sockaddr, 0, sizeof(dst_sockaddr));
    dst_sockaddr.sin_family = AF_INET;
    dst_sockaddr.sin_addr.s_addr = inet_addr("224.0.0.5");

    char* packet = (char*)malloc(65535);
    while (true) {
        if (to_exit) {
            break;
        }

        // add neighbor
        size_t packet_real_len = OSPFHDR_LEN + sizeof(OSPFHello) + 4 * interface->neighbor_list.size();

        /* OSPF Header */
        OSPFHeader* ospf_header = (OSPFHeader*)packet;
        ospf_header->version = 2;
        ospf_header->type = 1; // Hello Type
        ospf_header->packet_length = htons(packet_real_len);
        ospf_header->router_id = htonl(myconfigs::router_id);
        ospf_header->area_id = htonl(interface->area_id);
        ospf_header->checksum = 0;
        ospf_header->autype = 0;
        ospf_header->authentication[0] = 0;
        ospf_header->authentication[1] = 0;

        /* OSPF Hello */
        OSPFHello* ospf_hello = (OSPFHello*)(packet + OSPFHDR_LEN); 
        ospf_hello->network_mask = htonl(0xffffff00);
        ospf_hello->hello_interval = htons(10);
        ospf_hello->options = 0x02;
        ospf_hello->rtr_pri = 1;
        ospf_hello->router_dead_interval = htonl(40);
        ospf_hello->designated_router = htonl(interface->real_dr);
        ospf_hello->backup_designated_router = htonl(interface->real_bdr);

        /* OSPF Attach */
        uint32_t* ospf_attach = (uint32_t*)(packet + OSPF_HELLO_LEN);
        for (auto& nbr: interface->neighbor_list) {
            *ospf_attach++ = htonl(nbr->id);
        }
        // calculte checksum
        ospf_header->checksum = crc_checksum(ospf_header, packet_real_len);

        /* Send Packet */
        if (sendto(socket_fd, packet, packet_real_len, 0, (struct sockaddr*)&dst_sockaddr, sizeof(dst_sockaddr)) < 0) {
            perror("[Thread]SendHelloPacket: sendto");
        } 
    #ifdef DEBUG
        else {
            printf("[Thread]SendHelloPacket: send success\n");
        }
    #endif
        sleep(interface->hello_intervel);
    }

    // free(packet);
    pthread_exit(NULL);
}

// 在合适的位置包含线程库头文件
inline void* threadSendEmptyDDPackets(void* nbr) {
    Neighbor* neighbor = (Neighbor*)nbr;
    pthread_t thread_id = pthread_self(); // 获取线程ID

    printf("_______________________________\n"
           "|threadSendEmptyDDPackets START|\n"
           "|      Interface = %s     |\n"
           "|      Thread ID = %lu     |\n"
           "_______________________________\n",
           neighbor->host_interface->nic_name, thread_id);

    while (true) {
        int count_num = 0;
        if (neighbor->state != NeighborState::S_EXSTART || count_num > 5) {
            printf("______________________________\n"
                   "|threadSendEmptyDDPackets End|\n"
                   "|      Interface = %s    |\n"
                   "|      Thread ID = %lu     |\n"
                   "______________________________\n",
                   neighbor->host_interface->nic_name, thread_id);
            break;
        }
        OSPFDD ospf_dd;
        memset(&ospf_dd, 0, sizeof(ospf_dd));
        ospf_dd.interface_MTU = htons(neighbor->host_interface->mtu);
        ospf_dd.options = 0x02; // 8bit: | * | * | DC | EA | N/P | MC | E | * |  
        ospf_dd.sequence_number = neighbor->dd_seq_num;
        ospf_dd.b_I = ospf_dd.b_M = ospf_dd.b_MS = 1;
        printf("\n\n\n-------------------sender 190 ---------------------------\n"
               "|      COUNT = %d     |\n\n\n", count_num);
        sendPackets((char *)&ospf_dd, sizeof(ospf_dd), T_DD, neighbor->ip, neighbor->host_interface);
    #ifdef DEBUG
        printf("[Thread]SendEmptyDDPacket: send success, Thread ID = %lu\n", thread_id);
    #endif
        sleep(neighbor->host_interface->rxmt_interval); // normally 5
        count_num++;
    }
    
    pthread_exit(NULL);
}


inline void* threadSendLSRPackets(void* nbr) {
Neighbor* neighbor = (Neighbor*)nbr;
    
    while (true) {
        if (to_exit) {
            break;
        }

        pthread_mutex_lock(&neighbor->link_state_req_list_lock);
        if (neighbor->link_state_req_list.size() == 0) {
            neighbor->handle_loading_done();
            break;
        }

        char* lsr_packet = (char*)malloc(1024);
        int cnt = 0;
        // TODO: check number of link_state_req_list
        OSPFLSR* lsr_data = (OSPFLSR*) lsr_packet;
        for (auto& req_lsa_header: neighbor->link_state_req_list) {
            lsr_data->state_id = htonl(req_lsa_header.link_state_id);
            lsr_data->adverising_router = htonl(req_lsa_header.advertising_router);
            lsr_data->type = htonl(req_lsa_header.ls_type);

            lsr_data += 1;
            cnt += 1;
        }
        pthread_mutex_unlock(&neighbor->link_state_req_list_lock);
        sendPackets(lsr_packet, sizeof(OSPFLSR)*cnt, T_LSR, neighbor->ip, neighbor->host_interface);
        #ifdef DEBUG
            printf("[Thread]SendLSRPacket: send success\n");
        #endif
        free(lsr_packet);

        sleep(neighbor->host_interface->rxmt_interval);
    }

    pthread_exit(NULL);
}

#endif // PACKET_MANAGE_H
