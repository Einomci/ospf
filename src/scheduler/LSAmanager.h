#ifndef LSA_MANAGE_H
#define LSA_MANAGE_H

#include "../unit/lsdb.h"
#include "../unit/interface.h"
#include "../config/setting.h"
#include <iostream>
#include <vector>

// Function declarations
inline LSARouter* genRouterLSA(std::vector<Interface*>& sel_interfaces);
inline LSANetwork* genNetworkLSA(Interface *interface);

inline void onGeneratingRouterLSA();
inline void onGeneratingNetworkLSA(Interface* interface);
inline std::string uint32ToIpv4String_LSA_manager(uint32_t ip) {
    struct in_addr ip_addr;
    ip_addr.s_addr = htonl(ip); // Convert to network byte order
    return std::string(inet_ntoa(ip_addr)); // Convert to dotted-decimal string
}

// Function definitions

inline void onGeneratingRouterLSA() {
    #ifdef DEBUG
        printf("...generating a router lsa...\n");
    #endif
    pthread_mutex_lock(&lsdb.router_lock);
    LSARouter* router_lsa = genRouterLSA(myconfigs::interfaces);

    LSARouter* lsa_find = lsdb.getRouterLSA(myconfigs::router_id, myconfigs::router_id);
    bool is_added = false;
    // assure its comparible
    if (lsa_find == nullptr) {
        /* lsdb do not contain this lsa: add the lsa */
        #ifdef DEBUG
            printf("it is a new router lsa, insert to lsdb\n");
        #endif
        lsdb.router_lsas.push_back(router_lsa);
        // is_added = true;
    } else if (*router_lsa > *lsa_find) {
        #ifdef DEBUG
            printf("it is a newer router lsa, insert to lsdb, remove old one\n");
        #endif
        lsdb.delLSA(lsa_find->lsa_header.link_state_id, lsa_find->lsa_header.advertising_router, LSA_ROUTER);
        lsdb.router_lsas.push_back(router_lsa);
        is_added = true;
    }
    // consider -- so why not use <set>
    if (is_added) {
    #ifdef DEBUG
        printf("...try to flood lsa\n");
    #endif
        lsdb.floodLSA(router_lsa, myconfigs::interfaces);
    }
    pthread_mutex_unlock(&lsdb.router_lock);
}

inline void onGeneratingNetworkLSA(Interface* interface) {
    #ifdef DEBUG
        printf("_________________NETWORK_________________\n");
    #endif
    pthread_mutex_lock(&lsdb.network_lock);
    LSANetwork* network_lsa = genNetworkLSA(interface);
    LSANetwork* lsa_find = lsdb.getNetworkLSA(myconfigs::router_id, interface->ip);
    bool is_added = false;
    if (lsa_find == nullptr || !(*lsa_find == *network_lsa)) {
        /* lsdb do not contain this lsa: add the lsa */
        #ifdef DEBUG
            printf("it is a new network lsa, insert to lsdb\n");
        #endif
        lsdb.network_lsas.push_back(network_lsa);  
    } else if (*network_lsa > *lsa_find) {
        #ifdef DEBUG
            printf("it is a newer network lsa, insert to lsdb, remove old one\n");
        #endif
        lsdb.delLSA(lsa_find->lsa_header.link_state_id, lsa_find->lsa_header.advertising_router, LSA_NETWORK);
        lsdb.network_lsas.push_back(network_lsa);  
        is_added = true;
    }

    if (is_added) {
    #ifdef DEBUG
        printf("...try to flood lsa\n");
    #endif
        lsdb.floodLSA(network_lsa, myconfigs::interfaces);
    }
    printf("_________________NETWORK_________________\n");
    pthread_mutex_unlock(&lsdb.network_lock);
}

inline LSARouter* genRouterLSA(std::vector<Interface*>& sel_interfaces) {
    LSARouter* router_lsa = new LSARouter();
    std::cout << "LSARouter->links contains all the interface link!!! but only one LSARouter!!!" << std::endl;
    router_lsa->lsa_header.ls_type = 1; // unnecessary now
    router_lsa->lsa_header.advertising_router = myconfigs::router_id;
    router_lsa->lsa_header.link_state_id = myconfigs::router_id;
    pthread_mutex_lock(&lsa_seq_lock);
        router_lsa->lsa_header.ls_sequence_number += lsa_seq_cnt;
        lsa_seq_cnt += 1;
    pthread_mutex_unlock(&lsa_seq_lock);

    for (auto& interface: sel_interfaces) {
        if (interface->state == InterfaceState::S_DOWN) {
            continue;   // pass down interface
        }

        LSARouterLink link;
        link.metric = interface->cost;
        link.tos_num = 0;
        if ((interface->state != InterfaceState::S_WAITING) &&
            (interface->dr == interface->ip || interface->find_neighbor(interface->dr)->state == NeighborState::S_FULL)
            ) {
            link.type = L_TRANSIT;
            link.link_id = interface->dr;
            link.link_data = interface->ip;
        } else {
            link.type = L_STUB;
            link.link_id = interface->ip & interface->mask;
            link.link_data = interface->mask;
            link.router_id = interface->neighbor_list.front()->id;
        }
        printf("GenRouterLSA push_back link cost = %d\n", link.metric);
        printf("GenRouterLSA push_back link id   = %s\n", uint32ToIpv4String_LSA_manager(link.link_id).c_str());
        printf("GenRouterLSA push_back link data = %s\n", uint32ToIpv4String_LSA_manager(link.link_data).c_str());
        router_lsa->links.push_back(link);
        printf("\n\n\n||| rlsa->links.size = %d |||\n\n\n", router_lsa->links.size());
    }
    router_lsa->link_num = router_lsa->links.size();
    
    return router_lsa;
}

inline LSANetwork* genNetworkLSA(Interface *interface) {
    LSANetwork* network_lsa = new LSANetwork();

    network_lsa->lsa_header.ls_type = 2; // unnecessary now
    network_lsa->lsa_header.advertising_router = myconfigs::router_id;
    network_lsa->lsa_header.link_state_id = interface->ip;
    pthread_mutex_lock(&lsa_seq_lock);
        network_lsa->lsa_header.ls_sequence_number += lsa_seq_cnt;
        lsa_seq_cnt += 1;
    pthread_mutex_unlock(&lsa_seq_lock);

    for (auto& p_neighbor: interface->neighbor_list) {
        if (p_neighbor->state == NeighborState::S_FULL) {
            network_lsa->attached_routers.push_back(p_neighbor->ip);
        }
    }

    return network_lsa;
}

#endif // LSA_MANAGE_H
