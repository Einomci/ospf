#ifndef INTERFACE_H
#define INTERFACE_H

#include "neighbor.h"
#include "../scheduler/Retransmitter.h"

#include "../tools/utils.h"

void* start_wait_timer(void* intf);

class Interface {
public:
    NetworkType    type  = NetworkType::T_BROADCAST;  // default broadcast in ensp
    InterfaceState state = InterfaceState::S_DOWN;
    in_addr_t   ip;
    const char* nic_name;
    int         nic_id;

    uint32_t    mask = 0xffffff00;
    uint32_t    area_id = 0;

    uint32_t    hello_intervel = 10;
    uint32_t    router_dead_interval = 40;  // 4 times of hello_intervel
    uint32_t    rxmt_interval  = 5;
    uint32_t    intf_trans_delay = 1;
    uint32_t    router_priority = 1;

    Retransmitter rxmtter = Retransmitter(this);  // use for rxmt

    uint32_t    cost = 1;
    uint32_t    mtu = 1500;

    uint32_t    dr = 0;
    uint32_t    bdr = 0;
    uint32_t    real_dr = 0;
    uint32_t    real_bdr = 0;
    std::list<Neighbor*> neighbor_list;
    
    Interface()=default;
    ~Interface();
    Neighbor* find_neighbor(in_addr_t ip);
    Neighbor* create_neighbor(in_addr_t ip);
    /* event */
    void trigger_init();
    void trigger_timer();
    void trigger_bdr_detected();
    void trigger_neighbor_change();
private:
    void trigger_dr_select();
    void handleStateTransition(const char* messagePrefix, bool shouldTriggerDrSelect, const char* stateFrom);
    void updateState(InterfaceState newState, const char* stateName);
    void createDetachedThread(pthread_t& thread, void* (*start_routine)(void*), void* arg);
    Neighbor* findNeighborOrCreate(in_addr_t ip, bool createIfNotFound);
    void clearNeighborList();
};

#endif // INTERFACE_H