#include "../config/setting.h"
#include "interface.h"
#include "lsdb.h"
#include "../scheduler/LSAmanager.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>     // memset()
#include <stdint.h>
#include <iostream>
#include <assert.h>

#include <unistd.h>     // close()
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <pthread.h>

/* socket */ 
#include <sys/socket.h>
#include <netinet/in.h> // ntoh hton ...
#include <netinet/ip.h>
#include <netinet/if_ether.h> // ETH_P_IP ... ethernet frame

/* kernel */
#include <sys/ioctl.h>  // ioctl()
#include <net/if.h>     // ifr
#include <arpa/inet.h>  // inet_addr inet_ntoa()
#include <list>
#include <vector>
#include <algorithm>

/* priority first, router id second */
bool compareNeighbor(Neighbor* a, Neighbor* b) {
    return (a->priority != b->priority) ? (a->priority > b->priority) 
           : (a->id != b->id) ? (a->id > b->id)
           : (a->ip > b->ip);
}


void* start_wait_timer(void* intf) {
    Interface* interface = static_cast<Interface*>(intf);
    const int wait_time = 40; // time not sure
    for (int i = 0; i < wait_time; ++i) {
        sleep(1);
        // 假设我们需要在每一秒钟进行一些检查
        if (interface->state == InterfaceState::S_DOWN) {
            // 如果在等待期间，接口状态变为S_DOWN，则中止计时
            return nullptr;
        }
    }
    interface->trigger_timer();
}

Interface::~Interface() {
    clearNeighborList();
}

void Interface::clearNeighborList() {
    for (auto& neighbor : neighbor_list) {
        delete neighbor;
    }
    neighbor_list.clear();
}

Neighbor* Interface::find_neighbor(in_addr_t ip) {
    return findNeighborOrCreate(ip, false);
}

Neighbor* Interface::create_neighbor(in_addr_t ip) {
    return findNeighborOrCreate(ip, true);
}

Neighbor* Interface::findNeighborOrCreate(in_addr_t ip, bool createIfNotFound) {
    auto it = std::find_if(neighbor_list.begin(), neighbor_list.end(),
                           [ip](Neighbor* neighbor) { return neighbor->ip == ip; });
    if (it != neighbor_list.end()) {
        return *it;
    }
    if (createIfNotFound) {
        Neighbor* new_neighbor = new Neighbor(ip, this);
        neighbor_list.push_back(new_neighbor);
        return new_neighbor;
    }
    return nullptr;
}
void Interface::trigger_dr_select() {
    printf("\n\tstart electing DR/BDR...\n");

    // Initialize candidates list with self
    std::list<Neighbor*> candidates;
    Neighbor self(this->ip, this); 
    self.id = myconfigs::router_id;
    self.ndr = this->dr;
    self.nbdr = this->bdr;
    self.priority = this->router_priority; 
    candidates.push_back(&self);

    // Select neighbors as candidates
    for (auto& p_neighbor : neighbor_list) {
        if (static_cast<uint8_t>(p_neighbor->state) >= static_cast<uint8_t>(NeighborState::S_2WAY) && 
            p_neighbor->priority != 0) {
            candidates.push_back(p_neighbor);
        }
    }

    // Select BDR
    Neighbor* bdr = nullptr;
    auto selectFittestNeighbor = [](std::vector<Neighbor*>& candidates) -> Neighbor* {
        if (!candidates.empty()) {
            std::sort(candidates.begin(), candidates.end(), compareNeighbor);
            return candidates[0];
        }
        return nullptr;
    };

    std::vector<Neighbor*> bdr_candidates_lv1;
    std::vector<Neighbor*> bdr_candidates_lv2;
    for (auto& p_neighbor : candidates) {
        if (p_neighbor->ndr != p_neighbor->id) {
            bdr_candidates_lv2.push_back(p_neighbor);
            if (p_neighbor->nbdr == p_neighbor->id) {
                bdr_candidates_lv1.push_back(p_neighbor);
            }
        }
    }

    bdr = selectFittestNeighbor(bdr_candidates_lv1);
    if (!bdr) {
        bdr = selectFittestNeighbor(bdr_candidates_lv2);
    }

    // Select DR
    std::vector<Neighbor*> dr_candidates;
    for (auto& p_neighbor : candidates) {
        if (p_neighbor->ndr == p_neighbor->id) {
            dr_candidates.push_back(p_neighbor);
        }
    }

    Neighbor* dr = selectFittestNeighbor(dr_candidates);
    if (!dr) {
        dr = bdr; // If no explicit DR found, use BDR as DR
    }

    // Handle DR/BDR conflict and generate LSA
    if (((dr->ip == this->ip) ^ (this->dr == this->ip)) ||
        ((bdr->ip == this->ip) ^ (this->bdr == this->ip))) {
        // TODO: handle case where new DR/BDR conflicts with current state
    }

    if (dr->ip == this->ip && this->dr != this->ip) {
        onGeneratingNetworkLSA(this);
    }

    // Update interface DR/BDR
    this->dr = dr->ip;
    this->bdr = bdr->ip;

    printf("\tnew dr: %x\n", this->dr);
    printf("\tnew bdr: %x\n", this->bdr);
    printf("\tfinished\n");
}




void Interface::createDetachedThread(pthread_t& thread, void* (*start_routine)(void*), void* arg) {
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_create(&thread, &attr, start_routine, arg);
    pthread_attr_destroy(&attr);
}

void Interface::trigger_init() {
    printf("Interface %x received InterfaceUp ", this->ip);
    if (state == InterfaceState::S_DOWN) {
        pthread_t timer_thread;
        pthread_t rxmt_thread;
        createDetachedThread(timer_thread, start_wait_timer, this);
        createDetachedThread(rxmt_thread, Retransmitter::run, (void*)&(this->rxmtter));

        state = InterfaceState::S_WAITING;
        printf("and its state from DOWN -> WAITING.\n");
    } else {
        printf("and reject.\n");
    }
}

void Interface::updateState(InterfaceState newState, const char* stateName) {
    state = newState;
    printf("and its state from WAITING -> %s.\n", stateName);
}

void Interface::handleStateTransition(const char* messagePrefix, bool shouldTriggerDrSelect, const char* stateFrom) {
    printf("%s %x received %s ", messagePrefix, this->ip, stateFrom);
    if (shouldTriggerDrSelect) {
        trigger_dr_select();
        if (ip == dr) {
            updateState(InterfaceState::S_DR, "DR");
        } else if (ip == bdr) {
            updateState(InterfaceState::S_BACKUP, "BACKUP");
        } else {
            updateState(InterfaceState::S_DROTHER, "DROTHER");
        }
        onGeneratingRouterLSA();
    } else {
        printf("and reject.\n");
    }
}

void Interface::trigger_timer() {
    handleStateTransition("Interface", state == InterfaceState::S_WAITING, "start_wait_timer");
}

void Interface::trigger_bdr_detected() {
    handleStateTransition("Interface", state == InterfaceState::S_WAITING, "BackUpSeen");
}

void Interface::trigger_neighbor_change() {
    bool shouldTriggerDrSelect = (state == InterfaceState::S_DR ||
                                  state == InterfaceState::S_BACKUP ||
                                  state == InterfaceState::S_DROTHER);
    handleStateTransition("Interface", shouldTriggerDrSelect, "NeighborChange");
}