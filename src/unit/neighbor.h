#ifndef NEIGHBOR_H
#define NEIGHBOR_H

#include <stdint.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <deque>
#include <map>
#include "../tools/utils.h"

class Interface;

class Neighbor {
public:
    NeighborState state = NeighborState::S_DOWN;
    bool        is_master;
    uint32_t    dd_seq_num;

    /* last received DD data (of packet) from the neighbor
     *      [means]: host_interface last send, and only data oart of a dd packet for convenience
     */
    uint32_t    last_dd_seq_num;
    uint32_t    last_dd_data_len;
    char        last_dd_data[1024];

    /* neighbor details */
    uint32_t    id;  
    uint32_t    priority;  
    uint32_t    ip;
    uint32_t    ndr;    // neighbor's designated router
    uint32_t    nbdr;   // neighbor's backup designated router
    Interface*  host_interface;

    /* LSA information */
    std::map<uint32_t, uint32_t> link_state_rxmt_map;  // {dd_seq_num:id}
    std::deque<LSAHeader> db_summary_list; // already in net order
    std::deque<LSAHeader> link_state_req_list;
    pthread_mutex_t link_state_req_list_lock;
    pthread_t empty_dd_send_thread; // for negotiation of master/slave
    pthread_t lsr_send_thread;

    Neighbor(in_addr_t ip, Interface* intf);
    ~Neighbor();
    void reqListRemove(uint32_t link_state_id, uint32_t advertise_rtr_id);

    void handle_hello_received();  // neighbor's hello has been received
    void handle_2way_received(void* intf);
    void handle_1way_received();
    void handle_negotiation_done();
    void handle_seq_mismatch();
    void handle_exchange_done();
    void handle_bad_ls_request();
    void handle_loading_done();

private:
    void init_summary_list();
};


#endif // NEIGHBOR_H